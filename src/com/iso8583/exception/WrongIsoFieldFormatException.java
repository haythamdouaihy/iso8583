package com.iso8583.exception;


/**
 * 
 * @author MichelDahdah
 * 
 */
public class WrongIsoFieldFormatException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongIsoFieldFormatException(Object value, String expected) {
		super(String.format(" Expected data [%s]  \t Data [%s] NOT acceptable ", expected, value ));
	}
	public WrongIsoFieldFormatException(String s) {
		super(s);
	}
}
