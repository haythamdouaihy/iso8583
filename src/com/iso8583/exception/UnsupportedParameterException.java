package com.iso8583.exception;


/**
 * 
 * @author MichelDahdah
 * 
 */
public class UnsupportedParameterException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnsupportedParameterException(Object original, String domain) {
		super(String.format("Parameter %s is not supported in %s " , original.toString(), domain) );
	}
}
