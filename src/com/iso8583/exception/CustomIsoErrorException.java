package com.iso8583.exception;
/**
 * handles error specicific to the custom fields  (usualy field 48)
 * @author MichelDahdah
 *
 */
public class CustomIsoErrorException  extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomIsoErrorException(String s){
		super(s);
	}
	
}
