package com.iso8583.custom;

import java.io.IOException;
import java.net.Socket;
import java.text.ParseException;
import java.util.Random;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.iso8583.MessageFactory;
import com.iso8583.impl.SimpleTraceGenerator;
import com.iso8583.parse.ConfigParser;

/**
 * 
 * @author MichelDahdah
 * 
 */
public class ISOConnector {
	protected static final org.apache.log4j.Logger log = Logger
	.getLogger(ISOConnector.class);

	Socket s;
	MessageFactory mfact;

	/**
	 * 
	 * @param assignDate:
	 *                boolean that decided whether or not to set field 7
	 *                (transmition time) by default or not
	 * @throws IOException
	 */
	public ISOConnector(boolean assignDate) throws Exception {
		mfact = ConfigParser.createFromClasspathConfig("j8583Config.xml");
		mfact.setAssignDate(assignDate);
		mfact.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System
				.currentTimeMillis() % 10000)));
		
		int currentPort = this.getCurrentPort();  		
		s = new Socket(mfact.getIp(), currentPort);
	}
	
	/**
	 * returns a port to be used to connect to IEX randomly from a list of ports. 
	 * 
	 * @return
	 */
	private int getCurrentPort(){ 
		String[] availablePorts =mfact.getPort().split(",");
		int currentPortIndex=0;
		int currentPort =0;
		
		Random randomGenerator = new Random();
		currentPortIndex=randomGenerator.nextInt(availablePorts.length);
				
		currentPort = Integer.parseInt(availablePorts[currentPortIndex]);
		return currentPort;
	}

	/**
	 * 
	 * @param t :
	 *                message type of the iso message to be created
	 * @return : a new iso message of type <i>t</i>
	 */
	public CustomIsoMessage newRequest(MessageType t) {
		CustomIsoMessage m = mfact.newMessage(t);
		return m;
	}

	/**
	 * sends the request and waits on the socket for the response
	 * 
	 * @param m:
	 *                the message to be sent (the request)
	 * @return array list of iso messages (the response from the server)
	 * @throws IOException
	 * @throws ParseException
	 */

	public Vector<CustomIsoMessage> sendMessage(CustomIsoMessage m)
	throws Exception {
		Vector<CustomIsoMessage> response = new Vector<CustomIsoMessage>();
		log.debug("Sending request of type " + m.getMessageType());
		m.write(s.getOutputStream(), 2);
//		//BEGIN HAYTHAM UAT
//		byte[] lenHeader = new byte[100];
//		//END HAYTHAM UAT
		byte[] lenbuf = new byte[2];
		CustomIsoMessage resp = null;
		MessageType respType;
		int index = 0;
		while (s != null && s.isConnected()) {
//			//ISOHeader: "IH15                                              ";
//			//BEGIN HAYTHAM UAT 
//			if(new Integer(s.getInputStream().read(lenHeader)).toString().
//					equals(mfact.getIsoHeader(m.getType()))){
			
				s.getInputStream().read(lenbuf);
				resp = null;
				int size = ((lenbuf[0] & 0xff) << 8) | (lenbuf[1] & 0xff);
				byte[] buf = new byte[size];
				if (s.getInputStream().read(buf) == size) {
					log.debug(String.format("Parsing incoming from server : '%s'",
							new String(buf)));
					resp = mfact.parseMessage(buf, 0);
					respType = getResponseType(m, resp);
					log.info("\n\n **respType***"+respType+"********** \n\n");
					resp.setMessageType(respType, false);
					response.add(index, resp);
					index++;
					if (resp.getType() == 0x210 || resp.getType() == 0x810) {
						s.close();
						s = null;
					}
				} else {
					s.close();
					s = null;
				}
//			} else{
//				log.info("\n\n **INCORRECT RESPONSE HEADER********** \n\n");
//				s.close();
//				s = null;
//			}
			//END HAYTHAM UAT
		}
		return response;
	}

	private MessageType getResponseType(CustomIsoMessage req,
			CustomIsoMessage res) {
		if (req.getMessageType() == MessageType.ISO800_AUTHENTICATION) {
			return MessageType.ISO810_AUTHENTICATION;
		} else if (req.getMessageType() == MessageType.ISO800_CUSTOMERINFORMATION){
			return MessageType.ISO810_CUSTOMERINFORMATION;
		} else if (req.getMessageType() == MessageType.ISO200_ACCOUNTSUMMARYCASAFCA) {
			if (res.getType() == 0x210)
				return MessageType.ISO210_CASAFCASUMMARY;
			else
				return MessageType.ISO211_CASAFCASUMMARY;
		} else if (req.getMessageType() == MessageType.ISO200_ACCOUNTSUMMARYFDFCFD) {
			if (res.getType() == 0x210)
				return MessageType.ISO210_FDFCFDSUMMARY;
			else
				return MessageType.ISO211_FDFCFDSUMMARY;
		} else if (req.getMessageType() == MessageType.ISO200_ACCOUNTSUMMARYLOAN) {
			if (res.getType() == 0x210)
				return MessageType.ISO210_LOANSUMMARY;
			else
				return MessageType.ISO211_LOANSUMMARY;
		} else if (req.getMessageType() == MessageType.ISO200_FUNDTRANSFER) {
//			if (res.getType() == 0x210)
			return MessageType.ISO210_FUNDTRANSFER;
//			else
//			return MessageType.ISO211_FUNDTRANSFER;	
		} else if (req.getMessageType() == MessageType.ISO200_BILLPAYMENT) {
//			if (res.getType() == 0x210)
			return MessageType.ISO210_BILLPAYMENT;
//			else
//			return MessageType.ISO211_BILLPAYMENT;	    	    
		} else if (req.getMessageType() == MessageType.ISO200_IBGTRANSFER) {
//			if (res.getType() == 0x210)
			return MessageType.ISO210_IBGTRANSFER;
//			else
//			return MessageType.ISO211_BILLPAYMENT;	    	    
		} else if (req.getMessageType() == MessageType.ISO200_LOANPAYMENT) {
//			if (res.getType() == 0x210)
			return MessageType.ISO210_LOANPAYMENT;
//			else
//			return MessageType.ISO211_LOANPAYMENT;	    	    
		} else if (req.getMessageType() == MessageType.ISO200_CHEQUEBOOKREQUEST) {
			return MessageType.ISO210_CHEQUEBOOKREQUEST;
		}

		else if (req.getMessageType() == MessageType.ISO200_ACCOUNTDETAILCASAFCA) {

			return MessageType.ISO210_CASAFCADETAILS;
		} else if (req.getMessageType() == MessageType.ISO200_ACCOUNTDETAILFDFCFD) {

			return MessageType.ISO210_FDFCFDDETAILS;
		} else if (req.getMessageType() == MessageType.ISO200_ACCOUNTDETAILLOAN) {

			return MessageType.ISO210_LOANDETAILS;
		} else if (req.getMessageType() == MessageType.ISO200_TRANSACTIONHISTORY) {
			if (res.getType() == 0x210)
				return MessageType.ISO210_TRANSACTIONHISTORY;
			else
				return MessageType.ISO211_TRANSACTIONHISTORY;
		}
		return null;
	}
	
/*	public static void main(String args[]){
	      System.out.println(" & AND operator");
	      byte[] lenbuf = new byte[2];
	      lenbuf[0] = new byte("00");
	      lenbuf[1] = 0xEF;
	      int size = ((lenbuf[0] & 0xff) << 8) | (lenbuf[1] & 0xff);
	      int x = 1 | 1;
	      System.out.println("1 & 1 = " + x);
	}*/
}
