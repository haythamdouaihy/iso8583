package com.iso8583.custom;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.iso8583.IsoMessage;
import com.iso8583.IsoType;
import com.iso8583.exception.CustomIsoErrorException;
import com.iso8583.exception.WrongIsoFieldFormatException;

/**
 * 
 * @author MichelDahdah
 * 
 */
public class CustomIsoMessage extends IsoMessage {

	private MessageType messageType;

	/**
	 * 
	 * @param messageType : The message type that this message will be holding
	 * @param header : usually empty string 
	 */
	public CustomIsoMessage(MessageType messageType, String header) {
		super(header);
		if (messageType != null)
			this.messageType = messageType;
	}

	/**
	 *  
	 * @param bit: the field number of which there is multiple rows 
	 * @return the number of rows that are found in a specific field (ex: in account summary response , field 48 can contain 1 to 9 rows of CA SA FCA summary.
	 */
	public int getNumRows(int bit) {

		return (getField(bit).getLength() - messageType.getFirstRowOffset()) / getMessageType().getRowLength(bit);
	}

	/**
	 * returns the field present between bytes  field.offset and field.length in field number: field.bit 
	 * example: "12345abcde12345" 
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public String getCustomField(MessageField field) throws Exception {
		return getCustomField(field, 0);
	}
	public String getCustomField(MessageField field, int row)throws Exception{

		int numRows = getNumRows(field.getBit());
		if ( row >= numRows)
			throw new CustomIsoErrorException("ROW ["+row+"] out of range; This message contains only[" + numRows+"] ROWS ");
		if ((field.getISOType() != 0x210 && (messageType.getISOType() == 0x211 || messageType.getISOType() == 0x210))&& (field.getISOType() != messageType.getISOType()))
			throw new CustomIsoErrorException("FIELD " + field + " has type  ["+ field.getISOType() + "]  different from message type[" + getMessageType()+"] of current message");
		
		int offset = field.getOffset() + row * messageType.getRowLength(field.getBit());
		
		return ((String) getField(field.getBit()).getValue()).substring(offset, offset+ field.getLength());
	}
	/**
	 *
	 * @param field
	 * @return returns the custom field between bytes  field.offset and field.length in field number: field.bit  and FORMATS it removing trailing zeros or following spaces and returns either string or date format
	 * @throws Exception
	 */
	public Object getCField(MessageField field) throws Exception {
		return getCField(field, 0);
	}
	/**
	 *
	 * @param field
	 * @return returns the custom field between bytes  field.offset and field.length in field number: field.bit in row number row of that field  and FORMATS it removing trailing zeros or following spaces and returns either string or date format
	 * @throws Exception
	 */
	public Object getCField(MessageField field, int row) throws Exception {
		String value = getCustomField(field, row);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMDD");

		switch (field.isoType) {
		case ALPHA:
			return value.trim();
		case AMOUNT:
			return String.valueOf(Double.valueOf(value.substring(0, field.getLength() - 2) + "."+ value.substring(field.getLength() - 2)));
		case NUMERIC:
//			try {
//				return String.valueOf(Long.parseLong(value));
				return String.valueOf(value.trim());
//			} catch (NumberFormatException e) {
//				try {
//					return String.valueOf(Double.parseDouble(value));
//				} catch (NumberFormatException e1) {
//					throw e1;
//				}
//			}
		case DATE8:
			return sdf.parse(value.trim());
		default:
			throw new UnknownError("ERROR WHILE PARSING VALUE [" + value + "] OF TYPE ["+ field.isoType + "]");
		}

//		if (field.isoType == IsoType.ALPHA)
//			return value.trim();
//		else if (field.isoType == IsoType.AMOUNT) {
//			return Double.valueOf(value.substring(0, field.getLength() - 2) + "."
//					+ value.substring(field.getLength() - 2));
//		} else if (field.isoType == IsoType.NUMERIC) {
//			try {
//				return String.valueOf(Integer.parseInt(value));
//			} catch (NumberFormatException e) {
//				try {
//					return String.valueOf(Double.parseDouble(value));
//				} catch (NumberFormatException e1) {
//					throw e1;
//				}
//			}
//		} else {
//			FieldParseInfo f = new FieldParseInfo(field.isoType, field.length);
//			return f.parse(value.getBytes(), 0).getValue();
		

	}



	/**
	 * Used in SetCustomField to set a numeric field 
	 * 
	 * @param value a big decimal 
	 * @param len: length that this field has to formatted in
	 * @return  the value formated in length [len] by adding trailing zeroes where necessary.
	 */
	public String formatDouble(BigDecimal value, int len) {

		String sVal = value.toString();
		int decAt = sVal.indexOf(".");
		int numsBeforeDec = 0;
		if (decAt == -1) {
			numsBeforeDec = sVal.length();
		} else {
			numsBeforeDec = decAt;
		}
		if (numsBeforeDec > len) {
			// add logging ERROR here
			System.out.println("WARNING data might be lost ");
			return sVal.substring(0, len);
		}
		if (sVal.length() > len)
			return sVal.substring(0, len);

		String s = sVal;
		if (sVal.length() < len) {
			for (int i = 0; i < len - sVal.length(); i++) {
				s = "0" + s;
			}
		}
		return s;

	}

	public void setCustomField(MessageField field, Object value) throws Exception {
		setCustomField(field, value, 0);
	}
	/**
	 * 
	 * @param field  The field that is to be set
	 * @param value  The value of the field that is to be set 
	 * @param row    The row in which the field is to be st
	 * @throws Exception 
	 */
	public void setCustomField(MessageField field, Object value, int row) throws Exception {

		if ((field.isoType == IsoType.DATE10 || field.isoType == IsoType.DATE4 || field.isoType == IsoType.DATE8 || field.isoType == IsoType.DATE_EXP)
				&& !(value instanceof Date)) {
			throw new WrongIsoFieldFormatException(value, " DATE ");
		}

		if ((field.isoType == IsoType.ALPHA) && !(value instanceof String)) {
			throw new WrongIsoFieldFormatException(value, " STRING ");
		}
		if ((field.isoType == IsoType.NUMERIC)
				&& !((value instanceof Integer) || (value instanceof Double) || (value instanceof Float))) {
			throw new WrongIsoFieldFormatException(value, " LONG, INTEGER, DOUBLE ");
		}

		String val = "";
		if (value instanceof String)
			val = (String) value;

		if (value instanceof String && field.getLength() < val.length())
			throw new WrongIsoFieldFormatException(" TRYING TO SET [" + value + "]with length ["+((String)value).length()+"] in FIELD ["
					+ field + "] which has max  length  [" + field.getLength()+"]");

		if ((field.getISOType() != 0x210 && (messageType.getISOType() == 0x211 || messageType
				.getISOType() == 0x210))
				&& (field.getISOType() != messageType.getISOType())) {

			throw new CustomIsoErrorException("FIELD " + field + " has type  ["+ field.getISOType() + "]  different from message type[" + getMessageType()+"] of current message");


		}
		if (value instanceof Date) {
			val = field.isoType.format((Date) value);
		} else if (field.isoType == IsoType.AMOUNT) {
			val = field.isoType.format(BigDecimal.valueOf(Double.valueOf(value.toString())), field
					.getLength());

			// val = formatDouble(BigDecimal.valueOf((Double) value),
			// field.getLength());
		} else if (value instanceof Integer) {
			val = formatInteger((Integer) value, field.getLength());
		} else if (value instanceof Double) {
			val = formatDouble(BigDecimal.valueOf((Double) value), field.getLength());
			// val= field.isoType.format(BigDecimal.valueOf((Double) value),
			// field.getLength()) 
		} else
			val = field.isoType.format((String) value, field.getLength());

		if (val.length() != field.getLength()) {
			throw new InternalError("INTERNAL ERROR!!!! ");
		}

		int offset = field.getOffset() + row * messageType.getRowLength(field.getBit());
		int length = field.getLength();
//		log.info("\n\n\n *offset****"+offset+"************ \n\n\n");
//		log.info("\n\n\n *field****"+field+"************ \n\n\n");
//		log.info("\n\n\n *val****"+val+"************ \n\n\n");
		String oldVal = this.getField(field.getBit()).getValue().toString();
//		log.info("\n\n\n *oldVal****"+oldVal+"************ \n\n\n");
		String newVal = oldVal.substring(0, offset) + val + oldVal.substring(offset + length);
//		log.info("\n\n\n *newVal****"+newVal+"************ \n\n\n");
		this.setValue(field.getBit(), newVal, IsoType.LLLVAR, 0);

	}

	/**
	 * 
	 * @param value : the integer to be formated
	 * @param length: the length that the integer must be formatted 
	 * @return formated <i>value</i>   by adding trailing zeroes of length  <i>length</i> 
	 */
	private String formatInteger(Integer value, int length) {
		String s = value.toString();

		for (int i = 0; i < length - value.toString().length(); i++) {
			s = "0" + s;
		}
		return s;
	}

	/**
	 * 
	 * @return returns the message type of this isomessage
	 */
	public MessageType getMessageType() {
		return messageType;
	}

	/**
	 * sets the message type to the current isomessage
	 * @param messageType: the message type to be set
	 * @param isRequest boolean specifying whether this message is a request or a response.. if request it will set all LLLVAR fields and allocate space to be able to set fields to them.
	 */
	public void setMessageType(MessageType messageType, boolean isRequest) {
		this.messageType = messageType;
		this.setType(messageType.getISOType());

		if (isRequest) {
		
				for (int i = 0; i < messageType.getVarBits().length; i++)
					this.setValue(messageType.getVarBit(i), createDefaultString(messageType.getVarLength(i)
							* messageType.getRowCount(i) + this.messageType.getFirstRowOffset()), IsoType.LLLVAR, 0);
		}

	}

	/**
	 * 
	 * @param length : length of the string to be created
	 * @return: an empty string (spaces) of the length specified
	 */
	public String createDefaultString(int length) {
		StringBuffer s = new StringBuffer(length);
		for (int i = 0; i < length; i++) {
			s.append(" ");
		}
		return s.toString();
	}

};
