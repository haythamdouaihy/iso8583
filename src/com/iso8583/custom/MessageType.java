package com.iso8583.custom;

/**
 * THE MESSAGE TYPES REQUESTED BY BMMB WITH PROPERTIES ADDED FOR DYNAMIC FUNCTIONALITY
 * @author MichelDahdah
 * 
 */
public enum MessageType {

	ISO800_AUTHENTICATION(0x800, 0,  48, MessageField.ISO800OLDCARDNUMBER.getRightOffset()),
	ISO800_CUSTOMERINFORMATION(0x800, 0,  48, MessageField.ISO800CARDNUMBER.getRightOffset()),
	
	ISO810_AUTHENTICATION(0x810, 0,  48, MessageField.ISO810ACCOUNTTYPENUMBER.getRightOffset()),
	ISO810_CUSTOMERINFORMATION(0x810, 0,  48, MessageField.ISO810ACCOUNTTYPENUMBER.getRightOffset()),
	
	
	ISO200_ACCOUNTSUMMARYCASAFCA(0x200, 0,  48, MessageField.ISO200SUMFILLER.getRightOffset()),
	ISO200_ACCOUNTSUMMARYFDFCFD(0x200, 0,  48, MessageField.ISO200SUMFILLER.getRightOffset()),
	ISO200_ACCOUNTSUMMARYLOAN(0x200, 0,  48, MessageField.ISO200SUMFILLER.getRightOffset()),
	
	ISO200_ACCOUNTDETAILCASAFCA(0x200, 0,  48, MessageField.ISO200DETBANKID.getRightOffset()),
	ISO200_ACCOUNTDETAILFDFCFD(0x200, 0,  48, MessageField.ISO200DETBANKID.getRightOffset()),
	ISO200_ACCOUNTDETAILLOAN(0x200, 0,  48, MessageField.ISO200DETLOANFINANCINGTYPE.getRightOffset()),
	
	ISO200_FUNDTRANSFER(0x200, 0,  48, MessageField.ISO200FUNDFEECRPARTNERAC.getRightOffset()),
	ISO200_BILLPAYMENT(0x200, 0,  48, MessageField.ISO200BILLFEECRADDITIONALDESC.getRightOffset()),
	ISO200_IBGTRANSFER(0x200, 0, 48,  MessageField.ISO200IBGBANKCODE_TOACCOUNT.getRightOffset()),
	ISO200_CHEQUEBOOKREQUEST(0x200, 0, 48,  MessageField.ISO200CHEQUEBOOKACCOUNTBANKID.getRightOffset()),
	ISO200_ESASHARES(0x200, 0, 48,  MessageField.ISO200ESAUNITPRICE_SHAREINFO.getRightOffset()),
	ISO200_TRANSACTIONHISTORY(0x200, 0,  48, MessageField.ISO200TRXHISACCTBANKID.getRightOffset()),
	ISO200_LOANPAYMENT(0x200, 0, 48,  MessageField.ISO200LOANCRADDNDESC.getRightOffset()),
	
	ISO210_CASAFCASUMMARY(0x210, 0,  48, MessageField.ISO210SUMHOLDAMOUNT_CASAFCA.getRightOffset()),
	ISO210_FDFCFDSUMMARY(0x210, 0,  48, MessageField.ISO210SUMDMATURITYINSTRUCTIONS_FDFCFD.getRightOffset()),
	ISO210_LOANSUMMARY(0x210, 0,  48, MessageField.ISO210SUMCONTRACTDATE_LOAN.getRightOffset()),
	ISO210_CASAFCADETAILS(0x210, 0,  48, MessageField.ISO210DETHOLDAMOUNT_CASAFCA.getRightOffset()),
	ISO210_FDFCFDDETAILS(0x210, 0,  48, MessageField.ISO210DETMATURITYINSTRUCTION_FDFCFD.getRightOffset()),
	ISO210_LOANDETAILS(0x210, 0,  48, MessageField.ISO210DETMIAMONTHOVERDUE_LOAN.getRightOffset()),
	ISO210_FUNDTRANSFER(0x210, 0, 54,  MessageField.ISO210FUNDCURRENCYCODE_TOACC.getRightOffset()),
	ISO210_BILLPAYMENT(0x210, 0, 54,  MessageField.ISO210BILLCURRENCYCODE_FROMACC.getRightOffset()),
	ISO210_IBGTRANSFER(0x210, 0, 54,  MessageField.ISO210IBGCURRENCYCODE_FROMACC.getRightOffset()),
	ISO210_ESASHARES(0x200, 0, 54,  MessageField.ISO210ESACURRENCYCODE_FROMACC.getRightOffset()),
	ISO210_LOANPAYMENT(0x210, 0, 54,  MessageField.ISO210LOANPAYMENTCURRENCYCODE_TOACC.getRightOffset()),
	ISO210_CHEQUEBOOKREQUEST(0x210, 0, 54,  MessageField.ISO210CHEQUEBOOKREQUESTTEST2.getRightOffset()),// TESTINGG PURPOSE!!
	ISO210_TRANSACTIONHISTORY(0x210, MessageField.ISO210TRXHISTOTALDEBIT.getRightOffset(),  48, MessageField.ISO210TRXHISRUNNINGBALANCE.getRightOffset() - MessageField.ISO210TRXHISTOTALDEBIT.getRightOffset()),
	
	ISO211_CASAFCASUMMARY(0x211, 0,  48, MessageField.ISO210SUMHOLDAMOUNT_CASAFCA.getRightOffset()),
	ISO211_FDFCFDSUMMARY(0x211, 0,  48, MessageField.ISO210SUMDMATURITYINSTRUCTIONS_FDFCFD.getRightOffset()),
	ISO211_LOANSUMMARY(0x211, 0,  48, MessageField.ISO210SUMCONTRACTDATE_LOAN.getRightOffset()),
//	ISO211_FUNDTRANSFER(0x211, 0, 54,  MessageField.ISO210FUNDCURRENCYCODE_TOACC.getRightOffset()),
	ISO211_TRANSACTIONHISTORY(0x211, MessageField.ISO210TRXHISTOTALDEBIT.getRightOffset(),  48, MessageField.ISO210TRXHISRUNNINGBALANCE.getRightOffset() - MessageField.ISO210TRXHISTOTALDEBIT.getRightOffset()),
//	ISO211_BILLPAYMENT(0x211, 0, 54,  MessageField.ISO210BILLCURRENCYCODE_FROMACC.getRightOffset()),
//	ISO211_IBGTRANSFER(0x211, 0, 54,  MessageField.ISO210IBGCURRENCYCODE_FROMACC.getRightOffset()),
//	ISO211_ESASHARES(0x211, 0, 54,  MessageField.ISO210ESACURRENCYCODE_FROMACC.getRightOffset()),
//	ISO211_LOANPAYMENT(0x211, 0, 54,  MessageField.ISO200LOANCRADDNDESC.getRightOffset()),
	
	
	//ISO211_ACCOUNTSUMMARY(0x211, 0,  48, 113),
	ISO800_TESTENVIRONMENT(0x800, 0,  48, MessageField.ISO800ENCRYPTEDPWD.getRightOffset());
	//ISO211_FUNDTRANSFER(0x211, 0, 54,  113);

	


	/**
	 * @param ISOtype 							the type of this field	
	 * @param numbers						   	variable length parameters where for every two consecutive parameters, the first 
	 * is the index of the variable length bit that can be found in this message type , and the following is the expected size  of a single row of that variable length field 
	 */
	MessageType(int ISOtype,int firstRowOffset,  int... numbers) {
		this.firstRowOffset = firstRowOffset;
		varBits = new int[numbers.length / 2];
		varLengths = new int[numbers.length / 2];
		rowCounts = new int[numbers.length / 2];
		for (int i = 0; i < numbers.length; i++)
			if (i % 2 == 0)
				varBits[i / 2] = numbers[i];
			else
				varLengths[i / 2] = numbers[i];
		type = ISOtype;
		for (int i = 0; i < numbers.length / 2; i++) {
			rowCounts[i] = 1;
		}
	}
	private int firstRowOffset;
	private int type;		
	private int[] rowCounts;
	private int[] varBits;
	private int[] varLengths;

	
	/**
	 * sets the number of rows of field number <i>i</i>
	 * @param i, the field who's number of rows is to be set 
	 * @param rowNums
	 */
	public void setRowCount(int i, int rowNums) {
		rowCounts[i] = rowNums;
	}
	/**
	 * gets the number of rows expected in field number i
	 * @param i
	 * @return
	 */
	public int getRowCount(int i) {
		return rowCounts[i];
	}

	/**
	 * 
	 * @return the isotype of this message
	 */
	public int getISOType() {
		return type;
	}

	/**
	 * sets the type of the message
	 * @param type
	 */
	public void setType(int type) {
		this.type = type;
	}

	public int[] getVarBits() {
		return varBits;
	}

	public void setVarBits(int[] varBits) {
		this.varBits = varBits;
	}

	/**
	 * returns the variable length bit of index <i>index</i>
	 * @param index
	 * @return
	 */
	public int getVarBit(int index) {
		return getVarBits()[index];
	}

	public int[] getVarLengths() {
		return varLengths;
	}

	public void setVarLengths(int[] varLengths) {
		this.varLengths = varLengths;
	}

	public void setVarLength(int index, int length) {
		this.varLengths[index] = length;
	}

	public int getVarLength(int index) {
		return getVarLengths()[index];
	}

	public int getRowLength(int bit) {
		for (int i = 0; i < varBits.length; i++)
			if (varBits[i] == bit)
				return varLengths[i];
		return 0;
	}
	public int getFirstRowOffset() {
		return firstRowOffset;
	}
	public void setFirstRowOffset(int firstRowOffset) {
		this.firstRowOffset = firstRowOffset;
	}

}
